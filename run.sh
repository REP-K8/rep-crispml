#! /bin/bash

# This file documents all steps necessary to run the rep-crispml docker image locally

# docker run --rm -d -p 8000:8000 --name rep-crispml rep-crispml:latest
docker run --rm --network host --name rep-crispml rep-crispml:latest
