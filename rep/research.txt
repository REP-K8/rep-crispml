apiVersion: batch/v1
kind: Job
metadata:
  name: static-demo
spec:
  parallelism: 10
completions: 100
template:
  metadata:
  name: static-example
labels:
  jobgroup: static-example
spec:
  containers:
  - name: birthday
image: mtoto/mc-demo
command: ["Rscript", "monte-carlo.R"]
restartPolicy: Never

for p in $(kubectl get pods -l jobgroup=static-example -o name)
do
kubectl logs $p >> output.txt
done


apiVersion: batch/v1
kind: Job
metadata:
  name: par-demo-$ITEM
spec:
  template:
  metadata:
  name: par-example
labels:
  jobgroup: par-example
spec:
  containers:
  - name: birthday
image: mtoto/birthday-demo
command: ["Rscript", "birthday.R $ITEM"]
restartPolicy: Never

# Create seperate yaml file for each value of $ITEM
# create folder for jobs
mkdir jobs
# create job.yaml files
for i in {1..100}
do
  cat job.yaml | sed "s/\$ITEM/$i/" > ./jobs/job-$i.yaml
done


  # modeling function
  run_save_model <- function(method) {
    # load pkgs and data
    library(mlbench)
    library(caret)
    data("BostonHousing")
    # split data
    set.seed(123)
    train_index <- createDataPartition(BostonHousing$medv,1, p = .7)
    train <- BostonHousing[train_index[[1]],]
    # train model
    model <- train(medv ~., 
                   data = train, 
                   method = method)
    
    # upload to storage bucket
    file <- sprintf("%s_model.rds", method)
    saveRDS(model, file)
    googleCloudStorageR::gcs_upload(file, 
                                    name = file,
                                    bucket = "bostonmodels")
  }
