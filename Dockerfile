# https://hub.docker.com/r/trestletech/plumber/dockerfile
FROM trestletech/plumber
MAINTAINER at062084 <thomas.strehl@at.ibm.com>


# needed to avoid error on subsequent upt-get update
RUN echo 'deb http://deb.debian.org/debian bullseye main' > /etc/apt/sources.list

# install odbc libs plus some additional tools
RUN apt-get update -qq && apt-get install -y \
	unixodbc \
	unixodbc-dev \
        odbc-mariadb \
	mariadb-client  \
        libmariadbclient-dev \
        libxml2

RUN apt-get update -qq && apt-get install -y \
	ksh \
	procps \
	strace \
	findutils

RUN R -e "install.packages('caret')"
RUN R -e "install.packages('dplyr')"
RUN R -e "install.packages('magrittr')"
RUN R -e "install.packages('influxdbr')"
RUN R -e "install.packages('odbc')"
RUN R -e "install.packages('DBI')"
#RUN R -e "install.packages('ibmdbR')"
RUN R -e "install.packages('doMC')"
RUN R -e "install.packages('foreach')"
RUN R -e "install.packages('dbplyr')"
RUN R -e "install.packages('tidyr')"
RUN R -e "install.packages('caret')"
RUN R -e "install.packages('ggplot')"
RUN R -e "install.packages('randomForest')"
RUN R -e "install.packages('jsonlite')"
RUN R -e "install.packages('httr')"
RUN R -e "install.packages('RMariaDB')"

# install and configure DB2 driver for ODBC
# db2_host=db2w-qwtmrdf.eu-de.db2w.cloud.ibm.com
# ENV 	db2_host=dashdb-txn-sbox-yp-lon02-01.services.eu-gb.bluemix.net \
# New DB2 on cloud foundry. Created 20200213, around midnight
#ENV 	db2_host=db2w-qwtmrdf.eu-de.db2w.cloud.ibm.com \
#	db2_source=./dsdriver \
#	db2_dest=/opt/ibm/dsdriver
#COPY ${db2_source} ${db2_dest}/
#RUN   cd ${db2_dest}/ \
#	&& ./installDSDriver  \
#	&& cat ${db2_dest}/installDSDriver.log \
#	&& cat ${db2_dest}/db2profile \
#	&& . ./db2profile \
#	&& env | sort \
#	&& ${db2_dest}/bin/db2cli writecfg add -database BLUDB -host ${db2_host} -port 50000 \
#	&& ${db2_dest}/bin/db2cli writecfg add -database BLUDB -host ${db2_host} -port 50001 \
#	&& ${db2_dest}/bin/db2cli writecfg add -database BLUDB -host ${db2_host} -port 50001 -parameter "SecurityTransportMode=SSL" \
#	&& ${db2_dest}/bin/db2cli writecfg add -dsn BLUDB -database BLUDB -host ${db2_host} -port 50000 \
#	# && ${db2_dest}/bin/db2cli writecfg add -dsn BLUDB -database BLUDB -host ${db2_host} -port 50001 \
#	&& ${db2_dest}/bin/db2cli writecfg add -dsn dashdb -database BLUDB -host ${db2_host} -port 50000 \
#	# && ${db2_dest}/bin/db2cli writecfg add -dsn dashdb -database BLUDB -host ${db2_host} -port 50001 \
#        && mkdir /home/plumber \
#	&& echo ". ${db2_dest}/db2profile" >> /home/plumber/.profile \
#	&& cd ..
COPY ./odbc.ini /etc/odbc.ini
COPY ./odbcinst.ini /etc/odbcinst.ini

# copy R code and test data of crispml implementation into container
WORKDIR /crispml
COPY ./rep ./rep
COPY ./data ./data
COPY ./share ./share

# plump crispml api and start server on port 8000 (see trestletech image)
WORKDIR /crispml/rep
CMD ["/crispml/rep/crispml.R"]
